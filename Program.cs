﻿using System;
using System.IO;
using System.IO.Compression;
using ZstdNet;

namespace FSEQDump
{
    class Program
    {
        static UInt32 Read24Bits(BinaryReader br)
        {
            UInt32 res = 0;
            byte b1 = br.ReadByte();
            byte b2 = br.ReadByte();
            byte b3 = br.ReadByte();
            res = (((UInt32)b3) << 16) + (((UInt32)b2) << 8) + b1;

            return res;
        }

        static void DumpFSEQV1(BinaryReader br, UInt16 channelDataOffset)
        {
            byte flags = br.ReadByte();
            Console.WriteLine("Flags: 0b" + Convert.ToString(flags, 2));
            UInt16 universeCount = br.ReadUInt16();
            UInt16 universeSize = br.ReadUInt16();
            byte gamma = br.ReadByte();
            byte colour = br.ReadByte();
            UInt16 reserved1 = br.ReadUInt16();
        }

        static UInt32 DumpFSEQV2(BinaryReader br, UInt16 channelDataOffset, ref byte ct, ref UInt32 cb, ref long cbp)
        {
            UInt32 datasize = 0;
            byte reserved1 = br.ReadByte();
            byte compressionType = br.ReadByte();
            ct = (byte)(compressionType & 0x0F);
            switch (ct)
            {
                case 0: Console.WriteLine("Compression: None"); break;
                case 1: Console.WriteLine("Compression: ZStd"); break;
                case 2: Console.WriteLine("Compression: ZLib"); break;
                default: Console.WriteLine("Compression: Unknown"); break;
            }
            uint cbhigh = (uint)(compressionType & 0xF0);
            cbhigh = cbhigh << 4;
            uint cblow = (uint)br.ReadByte();
            uint compressionBlocks = cbhigh + cblow;
            cb = compressionBlocks;
            byte sparseRanges = br.ReadByte();
            byte flags = br.ReadByte();
            Console.WriteLine("Flags: 0b" + Convert.ToString(flags, 2));
            UInt64 uniqueId = br.ReadUInt64();
            Console.WriteLine("Unique Id: " + uniqueId.ToString());

            Console.WriteLine("\nCompression Blocks: " + compressionBlocks.ToString());

            cbp = br.BaseStream.Position;

            UInt32 offset = channelDataOffset;
            for (int i = 0; i < compressionBlocks; i++)
            {
                UInt32 frame = br.ReadUInt32();
                UInt32 length = br.ReadUInt32();
                Console.WriteLine("Block:" + (i + 1).ToString() + " Frame: " + frame.ToString() + " Length: " + length.ToString() + " Offset: " + offset.ToString() + " (0x" + Convert.ToString(offset, 16) + ")");
                offset += length;
                datasize += length;
            }

            Console.WriteLine("\nSparse Ranges: " + sparseRanges.ToString());

            for (int i = 0; i < sparseRanges; i++)
            {
                UInt32 startChannel = Read24Bits(br);
                UInt32 channels = Read24Bits(br);
                Console.WriteLine("Range:" + (i + 1).ToString() + " Channels: " + startChannel.ToString() + "-" + (startChannel + channels - 1).ToString() + " (" + channels.ToString() + ")");
            }

            return datasize;
        }

        static void DumpFSEQ(string fseq)
        {
            Console.WriteLine("\n\n***************************");
            Console.WriteLine(fseq);
            Console.WriteLine("***************************");

            BinaryReader br = new BinaryReader(File.Open(fseq, FileMode.Open, FileAccess.Read, FileShare.Read));

            byte[] token = br.ReadBytes(4);
            if (token.Length == 4 && (token[0] == 'F' || token[0] == 'P') && token[1] == 'S' && token[2] == 'E' && token[3] == 'Q')
            {
                Console.WriteLine("Token ok.");
            }
            else
            {
                Console.WriteLine("Token not present.");
                Console.WriteLine("Aborting.");
                return;
            }

            UInt16 channelDataOffset = br.ReadUInt16();
            Console.WriteLine("Channel Data Offset: " + channelDataOffset.ToString());
            byte minor = br.ReadByte();
            byte major = br.ReadByte();
            Console.WriteLine("Version: " + major.ToString() + ":" + minor.ToString());
            UInt16 fixedHeaderLength = br.ReadUInt16();
            Console.WriteLine("Fixed Header Length: " + fixedHeaderLength.ToString());
            UInt32 frameChannelCount = br.ReadUInt32();
            Console.WriteLine("Frame channel count: " + frameChannelCount.ToString());
            UInt32 frames = br.ReadUInt32();
            Console.WriteLine("Frames: " + frames.ToString());
            byte stepTime = br.ReadByte();
            Console.WriteLine("Step Time: " + stepTime.ToString() + "ms");
            float playTime = (float)(frames * stepTime) / 1000.0f;
            Console.WriteLine("Play Time: " + playTime.ToString("000.00") + "s " + ((int)playTime / 3600).ToString("D2") + ":" + 
                (((int)playTime % 3600) / 60).ToString("D2") + ":" + (playTime - ((int)(playTime / 60) * 60)).ToString("00.00"));

            UInt32 datasize = frames * frameChannelCount;

            byte compressionType = 0xFF;
            UInt32 compressionBlocks = 0xFFFF;
            long compressionBlockPos = 0;

            if (major == 1)
            {
                DumpFSEQV1(br, channelDataOffset);
            }
            else
            {
                datasize = DumpFSEQV2(br, channelDataOffset, ref compressionType, ref compressionBlocks, ref compressionBlockPos);
            }

            br.BaseStream.Seek(fixedHeaderLength, SeekOrigin.Begin);

            while (br.BaseStream.Position + 4 < channelDataOffset)
            {
                byte lowVariableHeaderLen = br.ReadByte();
                byte highVariableHeaderLen = br.ReadByte();
                char[] type = br.ReadChars(2);
                int vhl = ((int)highVariableHeaderLen << 8) + lowVariableHeaderLen - 4;
                String t = new String(type);
                char[] v = br.ReadChars(vhl);
                string s = new string(v);
                Console.WriteLine("Header: " + t + " Length: " + vhl.ToString() + " Value: " + s);
            }

            long calcFileSize = channelDataOffset + datasize;

            Console.WriteLine("File size: " + br.BaseStream.Length.ToString() + " (Variance: " + (br.BaseStream.Length - calcFileSize).ToString() + ")");
            Console.WriteLine("Fixed: " + fixedHeaderLength.ToString() + " + variable: " + (channelDataOffset - fixedHeaderLength).ToString() + " Data: " + datasize.ToString() + " = " + (channelDataOffset + datasize).ToString());
            Console.WriteLine("File data size: " + (br.BaseStream.Length - channelDataOffset).ToString() + " (Variance: " + ((br.BaseStream.Length - channelDataOffset) - datasize).ToString() + ")");

            DumpFrames(br, channelDataOffset, frameChannelCount, frames, major, compressionType, compressionBlocks, compressionBlockPos);
        }

        static byte[] ZStdDecompress(byte[] compressed)
        {
            byte[] decompressed = null;
            using (MemoryStream ms = new MemoryStream(compressed))
            {
                using (BinaryReader brd = new BinaryReader(ms))
                {
                    UInt32 decompressedSize = brd.ReadUInt32();
                    decompressed = new byte[decompressedSize];
                    using (ZstdNet.Decompressor dec = new ZstdNet.Decompressor())
                    {
                        dec.Unwrap(compressed, decompressed);
                    }
                }
            }

            return decompressed;
        }

        static byte[] ZLibDecompress(byte[] compressed)
        {
            // Skip the first two bytes (zlib header) and the last four bytes (zlib checksum)
            using (var compressedStream = new MemoryStream(compressed, 2, compressed.Length - 6))
            using (var deflateStream = new DeflateStream(compressedStream, CompressionMode.Decompress))
            using (var resultStream = new MemoryStream())
            {
                deflateStream.CopyTo(resultStream);
                return resultStream.ToArray();
            }
        }

        static void DumpFrames(BinaryReader br, UInt16 channelDataOffset, UInt32 frameChannelCount, UInt32 frames, byte major, byte compressionType, UInt32 compressionBlocks, long compressionBlockPos)
        {
            Console.WriteLine("");
            Console.WriteLine("Hit return to dump frame data.");
            Console.ReadLine();

            br.BaseStream.Seek(channelDataOffset, SeekOrigin.Begin);

            if (major == 1 || compressionType == 0)
            {
                for (int i = 0; i < frames; i++)
                {
                    Console.Write("Frame: " + (i + 1).ToString("D5") + " : ");
                    byte[] b = br.ReadBytes((int)frameChannelCount);
                    for (int k = 0; k < Math.Min(15, b.Length); k++)
                    {
                        Console.Write(b[k].ToString("X2") + " ");
                    }
                    Console.WriteLine("");
                }
            }
            else
            {
                UInt32 offset = channelDataOffset;

                for (int i = 0; i < compressionBlocks; i++)
                {
                    br.BaseStream.Seek(compressionBlockPos, SeekOrigin.Begin);
                    UInt32 frame = br.ReadUInt32();
                    UInt32 length = br.ReadUInt32();
                    if (length > 0)
                    {
                        compressionBlockPos = br.BaseStream.Position;
                        UInt32 nextFrame = br.ReadUInt32();
                        if (nextFrame == 0 || i == compressionBlocks - 1) nextFrame = frames;

                        br.BaseStream.Seek(offset, SeekOrigin.Begin);
                        byte[] compressed = br.ReadBytes((int)length);
                        byte[] decompressed = null;
                        if (compressionType == 1)
                        {
                            decompressed = ZStdDecompress(compressed);
                        }
                        else if (compressionType == 2)
                        {
                            decompressed = ZLibDecompress(compressed);
                        }

                        if (decompressed != null)
                        {
                            using (MemoryStream ms = new MemoryStream(decompressed))
                            {
                                using (BinaryReader brd = new BinaryReader(ms))
                                {
                                    for (int j = 0; j < nextFrame - frame; ++j)
                                    {
                                        byte[] b = brd.ReadBytes((int)frameChannelCount);
                                        Console.Write("Frame: " + (j + 1 + frame).ToString("D5") + " : ");
                                        for (int k = 0; k < Math.Min(15, b.Length); k++)
                                        {
                                            Console.Write(b[k].ToString("X2") + " ");
                                        }
                                        Console.WriteLine("");
                                    }
                                }
                            }
                        }
                        offset += length;
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Usage FSEQDump.exe <.fseq file>");
            }

            foreach(var f in args)
            {
                DumpFSEQ(f);
            }

            Console.WriteLine("");
            Console.WriteLine("Hit return to exit.");
            Console.ReadLine();
        }
    }
}
